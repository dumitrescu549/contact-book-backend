import os, django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'contact_book.settings')
django.setup()


import unittest
from unittest.mock import patch
from contact_book_backend.repositories.contact_book_repository import ContactBookRepository, Contact

contact_book_repository = ContactBookRepository()

class TestContactBookRepository(unittest.TestCase):
    # unit tests
    @patch('contact_book_backend.repositories.contact_book_repository.Contact.objects.create')
    @patch('contact_book_backend.repositories.contact_book_repository.Contact.save')
    def test_add_contact(self, mock_contact_book_save, mock_contact_book_create):
        # arrange
        input_name = "Andrei Dumitrescu"
        input_email = "dumitrescu549@gmail.com"
        input_phone_number = "0733974015"
        # mock for the save function of the ContactBook object (we don't care about the return, we just don't want the object to get saved in the DB)
        mock_contact_book_save.return_value = None
        # mock for the create function that should return a ContactBook object
        mock_contact_book_create.return_value = Contact(name=input_name, email=input_email, phone_number=input_phone_number)
        expected_value = mock_contact_book_create.return_value

        # act
        actual_value = contact_book_repository.add_contact(input_name, input_email, input_phone_number)

        # assert
        self.assertEqual(actual_value, expected_value)
