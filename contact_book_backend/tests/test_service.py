import os, django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'contact_book.settings')
django.setup()

import unittest
from unittest.mock import patch
from contact_book_backend.models.contact_model import Contact
from contact_book_backend.services.contact_book_service import ContactBookService

class TestContactBookRepository(unittest.TestCase):

    # integration tests

    @patch('contact_book_backend.repositories.contact_book_repository.ContactBookRepository')
    @patch('contact_book_backend.validators.contact_validator.ContactValidator')
    def test_valid_add_contact(self, mock_contact_validator, mock_contact_book_repository):
        # arrange
        input_uuid = '9a9a156a4af6492f99b45e5ddbd14f0e'
        input_name = "Andrei Dumitrescu"
        input_email = "dumitrescu549@gmail.com"
        input_phone_number = "0733974015"
        input_data = {
            'name': input_name,
            'email': input_email,
            'phone_number': input_phone_number
        }
        # mock instances of Contact Book Repository and Contact Validator that will be injected into ContactBookService
        contact_book_repository = mock_contact_book_repository.return_value
        contact_validator = mock_contact_validator.return_value
        # set the return value for the validate_add_contact function to None (the successful flow, in which all the data sent through the request is valid)
        contact_validator.validate_add_contact.return_value = None
        # set the return value for the add_contact function to the added contact object (the successful flow, in which the contact object is saved in DB)
        contact_book_repository.add_contact.return_value = Contact(uuid=input_uuid, name=input_name, email=input_email, phone_number=input_phone_number)
        # instantiate the service object with the mocks in the constructor
        contact_book_service = ContactBookService(contact_validator=contact_validator, contact_repository=contact_book_repository)
        # set the expected output and result
        expected_contact_response = {
            'uuid': input_uuid,
            'name': input_name,
            'email': input_email,
            'phone_number': input_phone_number
        }  
        expected_result = {
            'message':"Contact added successfully!",
            'contact': expected_contact_response,
            'error': False
        }
        
        # act 
        actual_result = contact_book_service.add_contact(input_data)

        # assert
        self.assertEqual(expected_result, actual_result)