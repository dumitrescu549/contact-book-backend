import unittest
from contact_book_backend.validators.contact_validator import ContactValidator

contact_validator = ContactValidator()

class TestContactValidator(unittest.TestCase):
    # unit tests
    def test_invalid_uuid(self):
        # arrange
        input_value = "012345"
        expected_value = False

        # act
        actual_value = contact_validator.is_valid_uuid(input_value)

        # assert
        self.assertEqual(actual_value, expected_value)
    
    def test_valid_uuid(self):
        # arrange
        input_value = "9a9a156a4af6492f99b45e5ddbd14f0e"
        expected_value = True

        # act
        actual_value = contact_validator.is_valid_uuid(input_value)

        # assert
        self.assertEqual(actual_value, expected_value)
    
    def test_invalid_phone_number(self):
        # arrange
        input_value = "073397401"
        expected_value = False

        # act
        actual_value = contact_validator.is_phone_number_valid(input_value)

        # assert
        self.assertEqual(actual_value, expected_value)
    
    def test_valid_phone_number(self):
        # arrange
        input_value = "0733974015"
        expected_value = True

        # act
        actual_value = contact_validator.is_phone_number_valid(input_value)

        # assert
        self.assertEqual(actual_value, expected_value)

    def test_invalid_phone_number_add_contact(self):
        # arrange
        input_value = {
            'name': 'Andrei Dumitrescu',
            'email': 'dumitrescu549@gmail.com',
            'phone_number': '073397401'
        }
        expected_value = {
            'message': 'The phone number you have entered is not valid !',
            'error': True
        }

        # act
        actual_value = contact_validator.validate_add_contact(input_value)

        # assert
        self.assertEqual(actual_value, expected_value)
    
    def test_valid_add_contact(self):
        input_value = {
            'name': 'Andrei Dumitrescu',
            'email': 'dumitrescu549@gmail.com',
            'phone_number': '0733974015'
        }
        expected_value = None

        # act
        actual_value = contact_validator.validate_add_contact(input_value)

        # assert
        self.assertEqual(actual_value, expected_value)