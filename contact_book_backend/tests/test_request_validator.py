import unittest
from contact_book_backend.validators.contact_request_validator import ContactRequestValidator

contact_request_validator = ContactRequestValidator()

class TestContactRequestValidator(unittest.TestCase):
    def test_non_existent_name_field_add_contact(self):
        # arrange
        input_value = {
            'email': 'dumitrescu549@gmail.com',
            'phone_number': '0733974015'
        }
        expected_value = {
            'message': 'Name field is mandatory.',
            'error': True
        }

        # act
        actual_value = contact_request_validator.validate_add_contact_request(input_value)

        # assert
        self.assertEqual(actual_value, expected_value)
