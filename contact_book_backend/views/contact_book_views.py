from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView

from ..services.contact_book_service import ContactBookService
from ..validators.contact_validator import ContactValidator
from ..validators.contact_request_validator import ContactRequestValidator
from ..repositories.contact_book_repository import ContactBookRepository

# repository instance
contact_repository = ContactBookRepository()
# validator instance
contact_validator = ContactValidator()
# contact request validator instance
contact_request_validator = ContactRequestValidator()
# service instance
contact_service = ContactBookService(contact_validator=contact_validator, contact_repository=contact_repository)

class ContactView(APIView):
    # POST /contacts/{name, email, phone_number}
    def post(self, request):
        data = JSONParser().parse(request)
        is_request_valid = contact_request_validator.validate_add_contact_request(data)
        if is_request_valid != None:
            return Response(is_request_valid)
        response = contact_service.add_contact(data)
        return Response(response)

    # GET /contacts/
    def get(self, request):
        response = contact_service.list_all_contacts()
        return Response(response)
    
    # PUT /contacts/{uuid, name, email, phone_number}
    def put(self, request):
        data = JSONParser().parse(request)
        is_request_valid = contact_request_validator.validate_edit_contact_request(data)
        if is_request_valid != None:
            return Response(is_request_valid)
        response = contact_service.edit_contact(data)
        return Response(response)
    
    # DELETE /contacts/{uuid}
    def delete(self, request):
        data = JSONParser().parse(request)
        is_request_valid = contact_request_validator.validate_delete_contact_request(data)
        if is_request_valid != None:
            return Response(is_request_valid)
        response = contact_service.delete_contact(data)
        return Response(response)
