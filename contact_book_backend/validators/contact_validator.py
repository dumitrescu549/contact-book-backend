import re
import uuid

class ContactValidator:

    def is_valid_uuid(self, value):
        try:
            uuid.UUID(str(value))
            return True
        except ValueError:
            return False

    def is_phone_number_valid(self, phone_number):
        # validates romanian phone numbers
        regex = r'^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$'
        
        if re.fullmatch(regex, phone_number):
            return True
        return False

    def is_email_valid(self, email):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

        if re.fullmatch(regex, email):
            return True
        return False

    def validate_add_contact(self, data):
        for key, value in data.items():
            if len(value) < 1:
                response = {
                    'message': f"You must enter your {key}!",
                    'error': True
                }
                return response
        
        if self.is_phone_number_valid(data['phone_number']) == False:
            response = {
                'message': 'The phone number you have entered is not valid !',
                'error': True
            }
            return response
        
        if self.is_email_valid(data['email']) == False:
            response = {
                'message': 'The email you have entered is not valid !',
                'error': True
            }
            return response
        return None

    def validate_edit_contact(self, data):
        
        if self.is_valid_uuid(data['uuid']) == False:
            response = {
                'message': 'You must enter a valid UUID!',
                'error': True
            }
            return response
        
        for key, value in data.items():
            if len(value) < 1:
                response = {
                    'message': f"You must enter your {key}!",
                    'error': True
                }
                return response
        
        if self.is_phone_number_valid(data['phone_number']) == False:
            response = {
                'message': 'The new phone number you have entered is not valid !',
                'error': True
            }
            return response
        
        if self.is_email_valid(data['email']) == False:
            response = {
                'message': 'The new email you have entered is not valid !',
                'error': True
            }
            return response

        return None
    
    def validate_delete_contact(self, data):
        
        if self.is_valid_uuid(data['uuid']) == False:
            response = {
                'message': 'You must enter a valid UUID!',
                'error': True
            }
            return response

        return None