class ContactRequestValidator:

    def validate_add_contact_request(self, data):
        if 'name' not in data.keys():
            response = {
                'message': 'Name field is mandatory.',
                'error': True
            }
            return response

        if 'email' not in data.keys():
            response = {
                'message': 'Email field is mandatory.',
                'error': True
            }
            return response
        
        if 'phone_number' not in data.keys():
            response = {
                'message': 'Phone number field is mandatory.',
                'error': True
            }
            return response
        return None

    def validate_edit_contact_request(self, data):
        if 'uuid' not in data.keys():
            response = {
                'message': 'UUID field is mandatory.',
                'error': True
            }
            return response
            
        if 'name' not in data.keys():
            response = {
                'message': 'Name field is mandatory.',
                'error': True
            }
            return response

        if 'email' not in data.keys():
            response = {
                'message': 'Email field is mandatory.',
                'error': True
            }
            return response
        
        if 'phone_number' not in data.keys():
            response = {
                'message': 'Phone number field is mandatory.',
                'error': True
            }
            return response
        return None
        
    def validate_delete_contact_request(self, data):
        if 'uuid' not in data.keys():
            response = {
                'message': 'UUID field is mandatory.',
                'error': True
            }
            return response
        return None