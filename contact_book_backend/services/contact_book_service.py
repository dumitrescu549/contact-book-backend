
class ContactBookService:

    def __init__(self, contact_validator, contact_repository):
        # instance used for validating different pieces of data sent through the requests
        self.contact_validator = contact_validator
        self.contact_repository = contact_repository

    # add a contact
    def add_contact(self, data):

        # validate data
        is_data_valid = self.contact_validator.validate_add_contact(data)

        # return an error message if anything went wrong with the request's body
        if is_data_valid != None:
            return is_data_valid
        
        # retreive the data from the request's body in separate variables
        name = data['name']
        email = data['email']
        phone_number = data['phone_number']

        #save the contact in DB
        contact = self.contact_repository.add_contact(name, email, phone_number)
        contact_response = {
            'uuid': contact.uuid,
            'name': contact.name,
            'email': contact.email,
            'phone_number': contact.phone_number
        }
        response = {
            'message':"Contact added successfully!",
            'contact': contact_response,
            'error': False
        }
        return response
    

    # list all the contacts available
    def list_all_contacts(self):
        
        # retrieve all the contacts available in DB
        all_contacts = self.contact_repository.get_all_contacts()

        # list which will store all the available contacts (or an empty list if no contacts are yet added)
        response_array = []
        
        # iterate through all the retrieved contacts from the DB
        for contact in all_contacts:
            # save them in a dictionary 
            response = {
                'uuid': contact.uuid,
                'name': contact.name,
                'email': contact.email,
                'phone_number': contact.phone_number 
            }
            # append the dictionary to the final response array
            response_array.append(response)

        return response_array
    
    
    # edit contact
    def edit_contact(self, data):

        # validate data
        is_data_valid = self.contact_validator.validate_edit_contact(data)
        
        # return an error message if anything went wrong with the request's body
        if is_data_valid != None:
            return is_data_valid
        
        # retreive the data from the request's body in separate variables
        uuid = data['uuid']
        name = data['name']
        email = data['email']
        phone_number = data['phone_number']

        # retrieve the contact that the user wants to edit
        contact = self.contact_repository.get_contact_by_uuid(uuid)

        # check if the contact exists in the DB        
        if not contact:
            # return an error message if the user tries to edit a contact that does not exist
            response = {
                'message':"Contact you want to edit does not exist!",
                'error': True
            }
            return response
        else:
            # if the user tries to "edit" a contact without updating any field, we won't just save the same user again in DB in order to avoid useless DB transactions
            if contact.name == name and contact.email == email and contact.phone_number == phone_number:
                response = {
                    'message': "The contact you tried to edit remained the same!",
                    'error': True
                }
                return response
            
            # save the edited contact in DB
            self.contact_repository.edit_contact(contact, name, email, phone_number)
            response = {
                'message':"Contact edited successfully!",
                'error': False
            }
            return response


    # delete contact
    def delete_contact(self, data):
        is_data_valid = self.contact_validator.validate_delete_contact(data)

        # return an error message if anything went wrong with the request's body
        if is_data_valid != None:
            return is_data_valid

        # retreive the data from the request's body in separate variables
        uuid = data['uuid'] 
        
        # retrieve the contact that the user wants to delete
        contact = self.contact_repository.get_contact_by_uuid(uuid)

        # check if the contact exists in the DB        
        if not contact:
            # return an error message if the user tries to delete a contact that does not exist
            response = {
                'message':"Contact you want to delete does not exist!",
                'error': True
            }
            return response
        else:
            self.contact_repository.delete_contact(contact)
            response = {
                'message':"Contact deleted successfully!",
                'error': False
            }
            return response