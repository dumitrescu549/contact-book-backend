from django.urls import path
from ..views.contact_book_views import ContactView

urlpatterns = [
    path('contacts', ContactView.as_view()),
]