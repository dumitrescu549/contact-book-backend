from django.apps import AppConfig


class ContactBookBackendConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'contact_book_backend'
