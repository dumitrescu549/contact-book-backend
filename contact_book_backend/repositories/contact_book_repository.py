from ..models.contact_model import Contact

class ContactBookRepository:

    # adds a contact to the DB
    def add_contact(self, input_name, input_email, input_phone_number):
        contact = Contact.objects.create(name=input_name, email=input_email, phone_number=input_phone_number) 
        contact.save()
        return contact

    # returns all the available contact in the DB
    def get_all_contacts(self):
        return Contact.objects.all()
    
    # returns a contact with the given UUID or None if it doesn't exist
    def get_contact_by_uuid(self, uuid):
        return Contact.objects.filter(uuid=uuid).first()
    
    # edit a contact
    def edit_contact(self, contact, name, email, phone_number):
        contact.name = name
        contact.email = email
        contact.phone_number = phone_number
        contact.save()
    
    # delete a contact
    def delete_contact(self, contact):
        contact.delete()