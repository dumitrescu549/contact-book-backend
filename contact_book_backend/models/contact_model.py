import uuid
from django.db import models

class Contact(models.Model):
    uuid = models.UUIDField(
        primary_key = True,
        default = uuid.uuid4,
        editable = False
    )
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)

    REQUIRED_FIELDS = []

    class Meta:
        app_label="contact_book_backend"